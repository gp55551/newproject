'''
Created on 01-Mar-2020

@author: gpatnaik
'''
# no need of datatype
a = 100 
print (a)

# Declare a variable and initialize it
f = 0
print(f)
# re-declaring the variable works
f = 'guru99'
print(f)

# can concate similar type
a = "Guru"
b = 99
print(a + str(b))

# Declare a variable and initialize it
f = 101
print(f)


# Global vs. local variables in functions
def someFunction():
# global f
    f = 'I am learning Python'
    print(f)


someFunction()
print(f)

print("=======================")
f = 101;
print(f)


# Global vs.local variables in functions
def someFunction1():
  global f
  print(f)
  f = "changing global variable"

someFunction1()
print(f)

print("=======================")


f = 11;
print(f)
del f
print(f)



