'''
Created on 27-Mar-2020

@author: gpatnaik
'''
##ifelse
x = 1 ;
def ifelse():
    #global x
    x = 3
    y = 2
    
    if(x>y):
        print("x is greater than y")
    else:
        print("y is " "greater than x")

ifelse()

##elif

def ifelseelif():
    #global x
    x = 2
    y = 2
    
    if(x>y):
        print("x is greater than y")
    elif(x==y):
        print("y is " "equal to x")
    else:
        print("y is " "greater than x")

ifelseelif()


##elifnested

def ifelseelifnested():
    #global x
    x = 3
    y = 2
    
    if(x>y):
        print("x is greater than y")
        if(x>y):
            print("yes")
        else:
            print("no")
    elif(x==y):
        print("y is " "equal to x")
    else:
        print("y is " "greater than x")

ifelseelifnested()



##switch

def SwitchExample(argument):
    switcher = {
        0: " This is Case Zero ",
        1: " This is Case One ",
        2: " This is Case Two ",
    }
    return switcher.get(argument, "nothing")

print(SwitchExample(1))


if __name__ == "__main__":
    argument = 1
    print (SwitchExample(argument))



#define a while loop
while(x <4):
    print(x)
    x = x+1


#for loop
for x in range(2,7):
    print(x)



#use a for loop over a collection
Months = ["Jan","Feb","Mar","April","May","June"]
for m in Months:
    print(m)

# use the break and continue statements
for x in range (10,20):
    if (x == 15): 
        break
    #if (x % 2 == 0) : continue
    print(x)


# use the break and continue statements
for x in range (10,20):
    #if (x == 15): break
    if (x % 5 == 0) : 
        continue
    print(x)


#Enumerate function is used for the numbering or 
#indexing the members in the list.



Months = ["Jan","Feb","Mar","April","May","June"]
for i, m in enumerate (Months):
    print(i,m)


for i in '123':
    print ("guru99",i)
 

#classes
















