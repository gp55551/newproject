

'''
Created on 14-Mar-2020

@author: gpatnaik
'''

#list
list1 = ["r",4,"Gaurav"]
print(list1)
print(list1[:])
print(list1[0]) #access element
print(list1[0:3]) #suBstring
print(len(list1)) #length
list1[1]=45 #change value
print(list1)



list_of_airlines=["AL1","AL2","AL3"]
print("Iterating the list using range()")
for index in range(0,len(list_of_airlines)):
    print(list_of_airlines[index])
print("Iterating the list using keyword in")
for airline in list_of_airlines:
    print(airline)


list_of_airlines=["AL1","AL2","AL3"]
airline="AL3"
if airline in list_of_airlines:
    print("Airline found")
else:
    print("Airline not found")
    

#tuple
lunch_menu=("Welcome Drink","Veg Starter","Non-Veg Starter","Veg Main Course",
            
            "Non-Veg Main Course","Dessert")
print(lunch_menu)
print(lunch_menu[0])


#Creating a string
pancard_number="AABGT6715H"
#Length of the string
print("Length of the PAN card number:", len(pancard_number))
#Concatenating two strings
name1 ="PAN "
name2="card"
name=name1+name2
print(name)
print("Iterating the string using range()")
for index in range(0,len(pancard_number)):
    print(pancard_number[index])
    
print("Iterating the string using keyword in")
for value in pancard_number:
    print(value)
print("Searching for a character in string")
if "Z" in pancard_number:
    print("Character present")
else:
    print("Character is not present")
#Slicing a string
print("The numbers in the PAN card number:", pancard_number[5:9])
print("Last but one 3 characters in the PAN card:",pancard_number[-4:-1])
#pancard_number[2]="A" #This line will result in an error, i.e., string is immutable
print(pancard_number)


#set

flight_set={500,520,600,345,520,634,600,500,200,200}
print(flight_set)

 
    
passengers_list=["George", "Annie", "Jack", "Annie", "Henry", "Helen", "Maria", "George", "Jack", "Remo"]
unique_passengers=set(passengers_list)
print(unique_passengers)




#dictionary - will store elements in key value pair

crew_details= { "Pilot":"Kumar","Co-pilot":"Raghav",
               "Head-Strewardess":"Malini","Stewardess":"Mala" }
print(crew_details["Pilot"])

for key,value in crew_details.items():
    print(key,":",value)











