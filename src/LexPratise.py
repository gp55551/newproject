

'''
Created on 13-Mar-2020

@author: gpatnaik
'''


a="infy"
b=20.127
c=10
print(a,b,c)
print(a,b,c,sep=":")
print(a,b,c,end=" ")
print(a,b,c)
print("b=%0.2f" %b)
print("c=%8d" %c)
print("c=%-8d" %c)


#input_var=input("please enter the value")
#print(input_var)

'''''
num1=10
num2="20"
result=num1+num2
print(result)
'''''
num1=10
num2="20"
result=num1+int(num2)
print(result)



a=10
if(a>0):
    print("positive integer")


'''
sum
'''
    
    
def numsum(n):
    sum = 0;
    while (n != 0):
        sum = sum + int(n % 10)
        n = int(n / 10)
    return sum
    
n = 143 
print("sum",numsum(n))

'''
if else
'''

Num = 4455
if((Num % 3 == 0) and (Num % 5 == 0)):
    print("Zoom")
elif(Num % 3 == 0):
    print("Zip")
elif(Num % 5 == 0):
    print("Zap")
else:
    print("Invalid")



num1=10
num2=20
num3=30
if(num1>num2):
    if(num1>num3):
        print("num1 is greater")
    else:
        print("num3 is greater")
elif(num2>num3):
    print("num2 is greater")
else:
    print("num3 is greater")

num=5
count=1
while (count <= num):
    print("the current number is:",count)
    count+=1
    
for number in 1,2,3,4,5:
    print("The current number is ",number)
    
start=1
end=10
step=2
for number in range (start, end, step):
    print("The current number is ", number)



for number in range(1,5):
    print ("The current number is ",number)
print("---------------------------")
for number in range(1,7,2):
    print ("The current number is ",number)
print("---------------------------")
for number in range(5,0,-1):
    print ("The current number is ",number)
    
    
  
number_of_passengers=5
number_of_baggage=2
security_check=True
for passenger_count in range(1, number_of_passengers+1):
    for baggage_count in range(1,number_of_baggage+1):
        if(security_check==True):
            print("Security check of passenger:", passenger_count, "-- baggage:", baggage_count,"baggage cleared")
        else:
            print("Security check of passenger:", passenger_count, "-- baggage:", baggage_count,"baggage not cleared")  
  
    
    



for passenger in "A","A", "FC", "C", "FA", "SP", "A", "A":
    if(passenger=="FC" or passenger=="FA"):
        print("No check required")
        continue
    if(passenger=="SP"):
        print("Declare emergency in the airport")
        break
    if(passenger=="A" or passenger=="C"):
        print("Proceed with normal security check")
    print("Check the person")
    print("Check for cabin baggage")




num=10
count=0
while(count <= num):
    if(count%2 == 0):
        pass
    else:
        print(count)
    count+=1





observe1="What's happening!!"
def passport_check(passport_no):
    observe4="actual copied to formal"
    observe5="func. execution starts"
    if(len(passport_no)==8):
        if(passport_no[0]>="A" and passport_no[0]<="Z"):
            status="valid"
        else:
            status="invalid"
    else:
        status= "invalid"
    observe6="func. execution ends"
    return status
observe2="function with formal arg."
observe3="calling with actual arg."
passport_status=passport_check("M9993471")
print("Passport is",passport_status)
#observe1,2,3,4,5,6 are temporary variables used to explain this concept



def find_square(n):
    return n*n


print(find_square(4))




def change_number(num):
    num+=10
def change_list(num_list):
    num_list.append(20)
num_val=10
print("*********effect of pass by value*********")
print("num_val before function call:", num_val)
change_number(num_val)
print("num_val after function call:", num_val)
print("-----------------------------------------------")
val_list=[5,10,15]
print("*********effect of pass by reference*********")
print("val_list before function call:", val_list)
change_list(val_list)
print("val_list after function call:", val_list)




num = [3,4,5,6]
num.append(45)
print(num)










