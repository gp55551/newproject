'''
Created on 01-Mar-2020

@author: gpatnaik
'''
var1 = "Guru99!"
var2 = "Software Testing"
print ("var1[0]:",var1[0])
print ("var2[1:5]:",var2[1:5])
print(var1*2)

x="Guru"
print (x[1])
x="Guru" 
print (x[1:3])
print ("u" in x)
print ("l" not in x)

name = 'guru'
number = 99
print ('%s %d' % (name,number))

x="Guru" 
y="99" 
print (x+y)


x = "Hello World!"
print(x[:]) 
print(x[:6]) 
print(x[0:6] + "-Guru99")


oldstring = 'I like Guru99' 
newstring = oldstring.replace('like', 'love')
print(newstring)


string="python at guru99"
print(string.upper())

string="python at guru99"        
print(string.capitalize())


string="PYTHON AT GURU99"
print(string.lower())



print(":".join("Python"))    


string="12345"        
print(''.join(reversed(string)))

word="guru99 career guru99"        
print(word.split(' '))


#In Python, Strings are immutable.
x = "Guru99"
x.replace("Guru99","Python")
print(x)


x = "Guru99"
x = x.replace("Guru99","Python")
print(x)






