'''
Created on 01-Apr-2020

@author: gpatnaik
'''

str1 = "Welcome to Guru99!"
after_strip = str1.strip()
print(after_strip)


#list cant be split
# mylist = ["a", "b", "c", "d"]
# print(mylist.strip()) 

str1 = "Welcome to Guru99!"
after_strip = str1.strip()
print(after_strip)

str2 = "Welcome to Guru99!"
after_strip1 = str2.strip()
print(after_strip1)

#t roemove the special char
str1 = "****Welcome to Guru99!****"
after_strip = str1.strip("*")
print(after_strip)

#to remove the char
str2 = "Welcome to Guru99!"
after_strip1 = str2.strip("99!")
print(after_strip1)

#willl not remove in the middle
str3 = "Welcome to Guru99!"
after_strip3 = str3.strip("to")
print(after_strip3)
