'''
Created on 29-Mar-2020

@author: gpatnaik
'''


# Example file for working with classes
class myClass():

    def method1(self):
        print("Guru99")
        
    def method2(self, someString):    
        print("Software Testing:" + someString)
  
      
def main():           
    # exercise the class methods
    c = myClass ()
    c.method1()
    c.method2(" Testing is fun")

  
if __name__ == "__main__":
    main()



#inheritance

# Example file for working with classes
class myClass1():
    def method1(self):
        print("Guru99")
        
  
class childClass(myClass1):
    def method1(self):
        myClass.method1(self);
        print ("childClass Method1")
        
    def method2(self):
        print("childClass method2")     
         
def main1():           
    # exercise the class methods
    c2 = childClass()
    c2.method1()
    c2.method2()
    
    c3 = myClass1()
    c3.method1()

if __name__== "__main__":
    main1()
    
    
#constructors
class User():
    name = ""

    def __init__(self, name):
        self.name = name

    def sayHello(self):
        print("Welcome to Guru99, " + self.name)

User = User("Alex")
User.sayHello()





    
    
    
    
    
